using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Firebase.Database;
using lisoft_data_management_restapi;
//using lisoft_data_management_restapi.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Nancy.Json;
using Newtonsoft.Json;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace lisoft_data_management_restapi
{
    public class Startup
    {
        private static HttpClient client = new HttpClient();
        private const string URL = "http://10.10.10.77:3003/api/communication";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            configure_mqtt();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void configure_mqtt()
        {
            MqttClient client = new MqttClient("www.vojtechpetrasek.com", 1883, false, null, null, MqttSslProtocols.None);
            client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
            string clientId = Guid.NewGuid().ToString();
            client.Connect(clientId);
            client.Subscribe(new string[] { "node/rawdata/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
        }

        public static double ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds);
        }



        private void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            var topic = e.Topic.ToString().Split('/');
            var eid = topic[2];
            var temp = topic[3];
            var voltage = topic[4];


            using (var httpClient = new HttpClient())
            {
                using (var request1 = new HttpRequestMessage(new HttpMethod("PUT"), "https://newagent-a153e.firebaseio.com/users/1/238asdb128bascd8dd890/actualTemp.json"))
                {
                    request1.Content = new StringContent($"{temp}");
                    var response1 = httpClient.SendAsync(request1).GetAwaiter().GetResult();
                }
            }

            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("PUT"), "https://newagent-a153e.firebaseio.com/users/1/238asdb128bascd8dd890/historicalData/" + ConvertToUnixTimestamp(DateTime.Now) + ".json"))
                {
                    request.Content = new StringContent($"{temp}");
                    var response = httpClient.SendAsync(request).GetAwaiter().GetResult();
                }
            }
        }
}

    } 
    

    public class Inputs
    {
        public string eid;
        public float temp;
        public float voltage;
        public DateTime timestamp;
    }


