﻿using Data.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using UI.ViewModels.Base;

namespace UI.ViewModels
{
    public class RecordItemViewModel : ViewModelBase
    {
        private readonly Record _record;

        public RecordItemViewModel(INavigationService navigationService, Record record) : base(navigationService)
        {
            _record = record;
        }
    }
}
