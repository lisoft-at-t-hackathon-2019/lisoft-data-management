﻿using Core.BussinessLogic.Managers;
using Data.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using UI.ViewModels.Base;

namespace UI.ViewModels
{
    public class StorageManagerViewViewModel : ViewModelBase
    {
        private readonly IRecordManager _recordManager;

        public StorageManagerViewViewModel(INavigationService navigationService, IRecordManager recordManager
            ): base(navigationService)
        {
            _recordManager = recordManager;
        }

        public DelegateCommand Add => new DelegateCommand(async () =>
        {
            var x = (await _recordManager.GetAllRecords());
            var DeserializejsonObservable = JsonConvert.DeserializeObject<Dictionary<string, float>>(x.ToString());

        });
        public DelegateCommand Remove => new DelegateCommand(() =>
        {
        });
    }
}
