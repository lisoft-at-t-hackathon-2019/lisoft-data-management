﻿using Data.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using UI.ViewModels.Base;

namespace UI.ViewModels
{
    public class StorageItemViewModel : ViewModelBase
    {
        private readonly Dictionary<string, Storage> _storage;

        public StorageItemViewModel(INavigationService navigationService, Dictionary<string,Storage> storage) : base(navigationService)
        {
            _storage = storage;
            EID = storage.FirstOrDefault().Key;
            Name = storage.FirstOrDefault().Value.Name;
        }

        public string Name { get; set; }
        public string EID { get; set; }

    }
}
