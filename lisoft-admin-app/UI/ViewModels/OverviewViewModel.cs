﻿using Core.BussinessLogic.Managers;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using UI.ViewModels.Base;

namespace UI.ViewModels
{
    public class OverviewViewModel : ViewModelBase
    {
        private readonly IRecordManager _recordManager;

        public OverviewViewModel(INavigationService navigationService, IRecordManager recordManager) : base(navigationService)
        {
            _recordManager = recordManager;
        }

        public DelegateCommand AppearingCommand => new DelegateCommand(async () =>
        {
            IsBusy = true;

            try
            {
                var x = _recordManager.GetAllRecords();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            IsBusy = false;
        });
    }
}
