﻿using Data.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using UI.ViewModels.Base;

namespace UI.ViewModels
{
    public class StockItemViewModel : ViewModelBase
    {
        private readonly Stock _stock;

        public StockItemViewModel(INavigationService navigationService, Stock stock) : base(navigationService)
        {
            _stock = stock;
        }
    }
}
