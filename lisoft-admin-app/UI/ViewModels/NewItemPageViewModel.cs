﻿using Core.BussinessLogic.Managers;
using Data.Models;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using UI.Factories;
using UI.ViewModels.Base;

namespace UI.ViewModels
{
    public class NewItemPageViewModel : ViewModelBase
    {
        private readonly IStockManager _stockManager;
        private readonly IStorageItemViewModelFactory _storageItemViewModelFactory;
        private readonly IStorageManager _storageManager;

        public NewItemPageViewModel(INavigationService navigationService, IStockManager stockManager, IStorageItemViewModelFactory storageItemViewModelFactory, IStorageManager storageManager) : base(navigationService)
        {
            _stockManager = stockManager;
            _storageItemViewModelFactory = storageItemViewModelFactory;
            _storageManager = storageManager;
           
        }
        public string Name { get; set; }
        public List<StorageItemViewModel> Storages {get; set;}
        public StorageItemViewModel Storage { get; set; }
        public int Amount { get; set; }
        public DelegateCommand Add => new DelegateCommand(async () =>
        {
            await _stockManager.AddStock(new Products() {Amount = Amount, Name = Name}, Storage.EID);

        });
        public DelegateCommand AppearingCommand => new DelegateCommand(async () =>
        {
            IsBusy = true;

            try
            {
                var storages = await _storageManager.GetAllStorages();
                var DeserializejsonObservable = JsonConvert.DeserializeObject<Dictionary<string, Storage>>(storages.ToString());
                Storages = DeserializejsonObservable.Select(item => _storageItemViewModelFactory.CreateStorageItemViewModel(DeserializejsonObservable)).ToList();
                RaisePropertyChanged(nameof(Storages));
                Storage = Storages.FirstOrDefault();
                RaisePropertyChanged(nameof(Storage));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            IsBusy = false;
        });
    }
}
