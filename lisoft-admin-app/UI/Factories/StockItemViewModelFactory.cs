﻿using Data.Models;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using UI.ViewModels;

namespace UI.Factories
{
    public class StockItemViewModelFactory : IStockItemViewModelFactory
    {
        private readonly INavigationService _navigationService;

        public StockItemViewModelFactory(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public StockItemViewModel CreateStockItemViewModel(Stock stock)
        {
            return new StockItemViewModel(_navigationService, stock);
        }
    }
}
