﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using UI.ViewModels;

namespace UI.Factories
{
    public interface IStockItemViewModelFactory
    {
        StockItemViewModel CreateStockItemViewModel(Stock stock);
    }
}
