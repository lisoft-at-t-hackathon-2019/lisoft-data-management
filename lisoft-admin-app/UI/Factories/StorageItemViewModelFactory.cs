﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Models;
using Prism.Navigation;
using UI.ViewModels;

namespace UI.Factories
{
    public class StorageItemViewModelFactory : IStorageItemViewModelFactory
    {
        private readonly INavigationService _navigationService;

        public StorageItemViewModelFactory(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }


        public StorageItemViewModel CreateStorageItemViewModel(Dictionary<string, Storage> storage)
        {
            return new StorageItemViewModel(_navigationService, storage);
        }
    }
}
