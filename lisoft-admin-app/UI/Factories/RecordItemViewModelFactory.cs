﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Models;
using Prism.Navigation;
using UI.ViewModels;

namespace UI.Factories
{
    public class RecordItemViewModelFactory : IRecordItemViewModelFactory
    {
        private readonly INavigationService _navigationService;

        public RecordItemViewModelFactory(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public RecordItemViewModel CreateRecordItemViewModel(Record record)
        {
            return new RecordItemViewModel(_navigationService, record);
        }
    }
}
