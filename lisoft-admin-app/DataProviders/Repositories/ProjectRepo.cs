﻿using Core.DataProviders.Repositories;
using Data.Models;
using Data.Static;
using Firebase.Auth;
using Firebase.Database;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DataProviders.Repositories
{
    public class ProjectRepo : IProjectRepo
    {
        private const string dburl = "https://newagent-a153e.firebaseio.com/";
        private const string historical = "users/1/238asdb128bascd8dd890/historicalData";
        private const string secret = "wQLFzHRybKsPkPAnm73Pr8vTAaSVuVwI6p8W1TY8";
        private const string data = "users/1/";
        private FirebaseClient firebase;

        public ProjectRepo()
        {
            this.firebase = new FirebaseClient(
               dburl,
               new FirebaseOptions
               {
                   AuthTokenAsyncFactory = () => Task.FromResult(secret)
               });
        }

        public Task<JObject> FetchAllstock()
        {
            throw new NotImplementedException();
        }

        public Task InsertStock(Products product, string storeid)
        {
            return Task.Run(async () =>
            {
                await firebase.Child($"{data}{storeid}/products").PostAsync(JsonConvert.SerializeObject(product));
            });
        }

        Task<JObject> IProjectRepo.FetchAllRecords()
        {
            try
            {
                return firebase.Child(historical).OnceSingleAsync<JObject>();
            }
            catch
            {
                return null;
            }

        }


        Task<JObject> IProjectRepo.FetchAllStorages()
        {
            return firebase.Child(data).OnceSingleAsync<JObject>();
        }
    }
}
