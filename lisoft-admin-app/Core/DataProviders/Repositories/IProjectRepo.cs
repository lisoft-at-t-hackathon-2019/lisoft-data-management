﻿using Data.Models;
using Firebase.Database;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.DataProviders.Repositories
{
    public interface IProjectRepo
    {
        Task<JObject> FetchAllstock();
        Task<JObject> FetchAllStorages();
        Task<JObject> FetchAllRecords();
        Task InsertStock(Products product,string freezerid);
    }
}
