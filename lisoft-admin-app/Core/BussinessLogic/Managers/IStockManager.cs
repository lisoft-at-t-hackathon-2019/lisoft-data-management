﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.BussinessLogic.Managers
{
    public interface IStockManager
    {
        Task<List<Stock>> GetStockByStorageEid();
        Task<List<Stock>> GetStockByName();
        Task AddStock(Products name, string storageid);
    }
}
