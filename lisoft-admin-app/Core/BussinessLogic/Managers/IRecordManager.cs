﻿using Data.Models;
using Firebase.Database;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.BussinessLogic.Managers
{
    public interface IRecordManager
    {
        Task<Record> GetLastRecord();
        Task<JObject> GetAllRecords();
    }
}
