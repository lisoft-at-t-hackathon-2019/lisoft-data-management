﻿using Data.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.BussinessLogic.Managers
{
    public interface IStorageManager
    {
        Task<JObject> GetAllStorages();
        Task<JObject> GetStoragesByName();
        Task<Storage> GetStorageById();
        Task AddItem();
    }
}
