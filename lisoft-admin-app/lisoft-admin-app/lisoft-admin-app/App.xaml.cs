﻿using Prism;
using Prism.Ioc;
using lisoft_admin_app.ViewModels;
using lisoft_admin_app.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using UI.Views;
using IOC;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace lisoft_admin_app
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService.NavigateAsync(nameof(MainTabbedPage));
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            iocContainer.RegisterTypes(containerRegistry);
        }
    }
}
