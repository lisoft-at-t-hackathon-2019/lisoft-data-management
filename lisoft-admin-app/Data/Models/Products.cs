﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public class Products
    {
        public int Amount { get; set; }
        public Dictionary<string, float> HistoricalData { get; set; }
        public int Minimal { get; set; }
        public string Name { get; set; }
    }
}
