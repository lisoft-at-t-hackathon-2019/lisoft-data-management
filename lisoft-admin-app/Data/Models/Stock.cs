﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public class Stock
    {
        [JsonProperty("ExpirationData")]
        public DateTime ExpirationData { get; set; }
        [JsonProperty("StorageEID")]
        public string StorageEID { get; set; }
    }
}
