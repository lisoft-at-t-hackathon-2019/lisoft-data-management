﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public class Storage
    {
        public string EID { get; set; }
        public float ActualTemp { get; set; }
        public Dictionary<string, float> HistoricalData { get; set; }
        public string Name { get; set; }
        public Products[] Products { get; set; }
    }
}
