﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public class Record
    {
        [JsonProperty("Temp")]
        public int Temp { get; set; }
    }
}
