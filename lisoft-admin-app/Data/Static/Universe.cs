﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Static
{
    public static class Universe
    {
        public const string URL = "10.10.10.77:3001/api/remote?type=";
        public const string record = "record";
        public const string stock = "stock";
        public const string storageDevices = "storageDevices";
    }
}
