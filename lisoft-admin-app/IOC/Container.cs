﻿using BussinessLogic.Managers;
using Core.BussinessLogic.Managers;
using Core.DataProviders.Repositories;
using DataProviders.Repositories;
using Prism.Ioc;
using System;
using System.Collections.Generic;
using System.Text;
using UI.Factories;
using UI.ViewModels;
using UI.Views;

namespace IOC
{
    public static class iocContainer
    {
        public static void RegisterTypes(IContainerRegistry containerRegistry)
        {
            //Pages
            containerRegistry.RegisterForNavigation<MainTabbedPage, MainTabbedPageViewModel>();
            containerRegistry.RegisterForNavigation<StorageManagerView, StorageManagerViewViewModel>();
            containerRegistry.RegisterForNavigation<Overview, OverviewViewModel>();
            containerRegistry.RegisterForNavigation<NewItemPage, NewItemPageViewModel>();

            //Factories
            containerRegistry.RegisterSingleton<IStockItemViewModelFactory, StockItemViewModelFactory>();
            containerRegistry.RegisterSingleton<IStorageItemViewModelFactory, StorageItemViewModelFactory>();
            containerRegistry.RegisterSingleton<IRecordItemViewModelFactory, RecordItemViewModelFactory>();

            //Managers
            containerRegistry.RegisterSingleton<IStorageManager, StorageManager>();
            containerRegistry.RegisterSingleton<IStockManager, StockManager>();
            containerRegistry.RegisterSingleton<IRecordManager, RecordManager>();

            //Repo
            containerRegistry.RegisterSingleton<IProjectRepo, ProjectRepo>();
        }
    }
}
