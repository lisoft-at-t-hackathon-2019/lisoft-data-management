﻿using Core.BussinessLogic.Managers;
using Core.DataProviders.Repositories;
using Data.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogic.Managers
{
    public class StorageManager : IStorageManager
    {
        private readonly IProjectRepo _projectRepo;

        public StorageManager(IProjectRepo projectRepo)
        {
            _projectRepo = projectRepo;
        }

        public Task AddItem()
        {
            throw new NotImplementedException();
        }

        public Task<JObject> GetStoragesByName()
        {
            throw new NotImplementedException();
        }

        Task<JObject> IStorageManager.GetAllStorages()
        {
            return _projectRepo.FetchAllStorages();
        }

        Task<Storage> IStorageManager.GetStorageById()
        {
            throw new NotImplementedException();
        }
    }
}
