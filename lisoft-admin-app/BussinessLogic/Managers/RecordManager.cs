﻿using Core.BussinessLogic.Managers;
using Core.DataProviders.Repositories;
using Data.Models;
using Firebase.Database;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogic.Managers
{
    public class RecordManager : IRecordManager
    {
        private readonly IProjectRepo _projectRepo;

        public RecordManager(IProjectRepo projectRepo)
        {
            _projectRepo = projectRepo;
        }
        Task<JObject> IRecordManager.GetAllRecords()
        {
            return _projectRepo.FetchAllRecords();
        }

        Task<Record> IRecordManager.GetLastRecord()
        {
            throw new NotImplementedException();
        }
    }
}
