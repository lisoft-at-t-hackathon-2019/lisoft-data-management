﻿using Core.BussinessLogic.Managers;
using Core.DataProviders.Repositories;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogic.Managers
{
    public class StockManager : IStockManager
    {
        private readonly IProjectRepo _projectRepo;

        public StockManager(IProjectRepo projectRepo)
        {
            _projectRepo = projectRepo;
        }

        public Task AddStock(Products name, string storageid)
        {
            return _projectRepo.InsertStock(name, storageid);
        }

        Task<List<Stock>> IStockManager.GetStockByName()
        {
            throw new NotImplementedException();
        }

        Task<List<Stock>> IStockManager.GetStockByStorageEid()
        {
            throw new NotImplementedException();
        }
    }
}
